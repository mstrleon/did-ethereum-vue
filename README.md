<img src="public/logoDP.png" height="40" alt="Digital Passport">

# Digital Passport

A Digital Identity App, based on Ceramic/IDX

## Features

- Uses Metamask or any wallet, compartible with WalletConnect
- Can use any Ethereum-based blockchain
- Passport user picture is uploaded to IPFS via Infura
- App is using Ceramic IDX / 3idConnect for creating DID
- Mobile-friendly

## Web application demo:

<a href="https://digital-passport.netlify.app/"> 
<img src="public/did.jpg" alt="demo pic" width="230">
</a>

https://digital-passport.netlify.app/





## Author

- [Leonid Anufriev](https://www.gitlab.com/mstrleon)


## Tech Stack

**Client:** Vue, Vuex, Quasar

**Blockchain** Ceramic/IDX, Infura
 


## License

[MIT](https://choosealicense.com/licenses/mit/)


## Roadmap

- Speed up app running

- Test in different environments

- Add more wallets


