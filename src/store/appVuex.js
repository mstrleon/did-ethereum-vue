import { appStates as statusList } from "../configs/status";
import {
  readProfile,
  updateProfile,
  exploreProfile
} from "src/helpers/DID-functions";

import {
  initWeb3,
  onConnect,
  provider,
  disconnectChain
} from "../helpers/web3-func";

const state = {
  loading: false,
  status: statusList.DISCONNECTED,
  appStates: statusList,

  userProfile: {},
  exploredProfile: {},
  useExplored: false,

  address: 0,
  connectionData: {
    chainData: {}
  },

  walletModalShow: false
};

const mutations = {
  SET_STATUS(state, status) {
    state.status = status;
  },
  SET_ADDRESS(state, address) {
    state.address = address;
  },
  SET_CONNECTION_DATA(state, connectionData) {
    state.connectionData = connectionData;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_PROFILE(state, val) {
    state.userProfile = { ...val };
  },
  SET_EXPLORED_PROFILE(state, val) {
    state.exploredProfile = { ...val };
  },
  SET_USE_EXPLORED_PROFILE(state, val) {
    state.useExplored = val;
  },
  SET_ACCOUNT(state, val) {
    state.account = val;
  },
  SET_WALLET_MODAL(state, val) {
    state.walletModalShow = val;
  }
};

const actions = {
  async initWeb3({ state, commit, dispatch }) {
    initWeb3();
  },

  async connectWalletAndProfile({ state, commit, dispatch }) {
    commit("SET_LOADING", true);
    let connectionData;
    try {
      connectionData = await onConnect(commit);
    } catch (e) {
      console.log(e);
      commit("SET_STATUS", state.appStates.DISCONNECTED);
      commit("SET_LOADING", false);
      return;
    }

    console.log("connectionData", connectionData);

    if (!connectionData) {
      // no connectins with wallet
      commit("SET_STATUS", state.appStates.DISCONNECTED);
      commit("SET_LOADING", false);
      return;
    }

    commit("SET_CONNECTION_DATA", connectionData);
    commit("SET_ADDRESS", connectionData.selectedAccount);
    commit("SET_STATUS", state.appStates.CONNECTED);

    // Subscribe to accounts change
    provider.on("accountsChanged", async () => {
      dispatch("disconnectWallet");
    });

    // Subscribe to chainId change
    provider.on("chainChanged", async () => {
      dispatch("disconnectWallet");
    });

    // Subscribe to networkId change
    provider.on("networkChanged", async () => {
      dispatch("disconnectWallet");
    });

    await dispatch("readProfile");
    commit("SET_LOADING", false);
  },

  async readProfile({ state, commit }) {
    commit("SET_LOADING", true);
    console.log("Quering profile for the address", state.address);
    const profile = await readProfile(state.address);
    console.log("profile read:", profile);

    if (profile === undefined) {
      commit("SET_STATUS", state.appStates.NEW_PROFILE);
      commit("SET_LOADING", false);
      return;
    }
    commit("SET_PROFILE", profile);
    commit("SET_STATUS", state.appStates.PROFILE_READ);
    commit("SET_LOADING", false);
  },

  async updateProfile({ state, commit }, profile) {
    commit("SET_LOADING", true);
    let did = "";
    try {
      did = await updateProfile(state.address, { ...profile });
    } catch (e) {
      console.error(e);
      commit("SET_LOADING", false);
      commit("SET_STATUS", state.appStates.PROFILE_READ);
      return;
    }

    commit("SET_PROFILE", { ...profile, ...did });
    commit("SET_LOADING", false);
    commit("SET_STATUS", state.appStates.PROFILE_READ);
  },

  async queryProfile({ state, commit }, didQuery) {
    console.log("looking up profile for DID: ", didQuery);
    commit("SET_LOADING", true);
    try {
      const resProfile = await exploreProfile(state.address, didQuery);
      commit("SET_EXPLORED_PROFILE", { ...resProfile });
      commit("SET_USE_EXPLORED_PROFILE", true);
    } catch (error) {
      console.warn("error ", error);
    }
    commit("SET_LOADING", false);
  },
  async disconnectWallet({ state, commit, dispatch }) {
    commit("SET_LOADING", false);
    await disconnectChain();
    commit("SET_STATUS", state.appStates.DISCONNECTED);
    await dispatch("connectWalletAndProfile");
    commit("SET_LOADING", true);
  }
};
const getters = {};

export default {
  state,
  mutations,
  actions,
  getters,
  namespaced: true
};
