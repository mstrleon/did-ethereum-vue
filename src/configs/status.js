export const appStates = {
  DISCONNECTED: 0,
  CONNECTED: 1,
  PROFILE_READ: 2,
  NEW_PROFILE: 3,
  NO_WALLET: -2,
  PROCEED_TO_METAMASK: -3,
  ERROR: -99
};

export default appStates;
