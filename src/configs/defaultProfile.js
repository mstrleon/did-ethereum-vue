export const defaultProfile = {
  name: "",
  image: "",
  //emoji: "",
  gender: "",
  birthdate: " ",
  residenceCity: "",
  residenceCountry: "",
  email: "",
  url: "",
  occupation: "",
  description: "",
  imageFile: ""
  //affilation: ""
};

export default defaultProfile;
