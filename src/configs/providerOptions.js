import WalletConnectProvider from "@walletconnect/web3-provider";

export const providerOptions = {
  walletconnect: {
    package: WalletConnectProvider,
    options: {
      // My infura project ID
      infuraId: "ab2675c23b2b427aa4e0108101b79883"
    }
  }
};
