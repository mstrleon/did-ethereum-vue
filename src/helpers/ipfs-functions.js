import { create } from "ipfs-http-client";
import axios from "axios";

export const ipfsBaseUrl = "https://ipfs.infura.io/ipfs/";

const ipfsClient = create("https://ipfs.infura.io:5001/api/v0");

export const uploadImage = async (_imageURL, _fileList) => {
  const res = await axios.get(_imageURL, { responseType: "arraybuffer" });
  const buffer = Buffer(res.data, "utf-8");
  try {
    const addedImage = await ipfsClient.add(buffer);
    return addedImage;
  } catch (err) {
    console.error(err);
  }
};
