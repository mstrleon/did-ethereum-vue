import CeramicClient from "@ceramicnetwork/http-client";
import ThreeIdResolver from "@ceramicnetwork/3id-did-resolver";

import { EthereumAuthProvider, ThreeIdConnect } from "@3id/connect";
import { DID } from "dids";
import { IDX } from "@ceramicstudio/idx";

import { provider } from "./web3-func";

const endpoint = "https://ceramic-clay.3boxlabs.com";

export async function readProfile(address) {
  const ceramic = new CeramicClient(endpoint);
  const idx = new IDX({ ceramic });

  try {
    const threeIdConnect = new ThreeIdConnect();
    const authProvider = new EthereumAuthProvider(provider, address);
    await threeIdConnect.connect(authProvider);
    const did = new DID({
      provider: threeIdConnect.getDidProvider(),
      resolver: {
        ...ThreeIdResolver.getResolver(ceramic)
      }
    });
    ceramic.setDID(did);
    await ceramic.did.authenticate();
    const data = await idx.get("basicProfile", ceramic.did._id);
    console.log("data from IDX.get: ", data);
    return { ...data, did: ceramic.did._id };
  } catch (error) {
    console.warn("error: ", error);
  }
}

export async function updateProfile(address, profile) {
  console.log("updating Profile");
  const ceramic = new CeramicClient(endpoint);
  const threeIdConnect = new ThreeIdConnect();
  const authProvider = new EthereumAuthProvider(provider, address);

  await threeIdConnect.connect(authProvider);

  const did = new DID({
    provider: threeIdConnect.getDidProvider(),
    resolver: {
      ...ThreeIdResolver.getResolver(ceramic)
    }
  });

  ceramic.setDID(did);
  await ceramic.did.authenticate();
  const idx = new IDX({ ceramic });
  await idx.set("basicProfile", {
    ...profile
  });
  console.log("Profile updated!");
  return { did: ceramic.did._id };
}

export async function exploreProfile(address, didValue) {
  console.log("quering profile", didValue);
  const ceramic = new CeramicClient(endpoint);
  const threeIdConnect = new ThreeIdConnect();
  const authProvider = new EthereumAuthProvider(provider, address);

  await threeIdConnect.connect(authProvider);

  const did = new DID({
    provider: threeIdConnect.getDidProvider(),
    resolver: {
      ...ThreeIdResolver.getResolver(ceramic)
    }
  });

  ceramic.setDID(did);
  await ceramic.did.authenticate();
  const idx = new IDX({ ceramic });
  const resProfile = await idx.get("basicProfile", didValue);

  console.log("resolved Profile", resProfile);
  return resProfile;
}
