export const formatAddress = (str) => {
  const strFirst = str.substring(0, 6);
  const strLast = str.slice(str.length - 4);
  return strFirst + "..." + strLast;
};
