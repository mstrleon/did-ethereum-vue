import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";
import Web3 from "web3";
import { getChain } from "evm-chains";

import { providerOptions } from "../configs/providerOptions";

// Web3modal instance
let web3Modal;

// Chosen wallet provider given by the dialog window
export let provider;

// Address of the selected account
let connectionData;

export async function initWeb3() {
  console.log("Initializing web3...");
  console.log("WalletConnectProvider is", WalletConnectProvider);
  console.log(
    "window.web3 is",
    window.web3,
    "window.ethereum is",
    window.ethereum
  );

  web3Modal = new Web3Modal({
    cacheProvider: true, // optional
    providerOptions, // required
    disableInjectedProvider: false // optional. For MetaMask / Brave / Opera.
  });

  await web3Modal.clearCachedProvider();

  console.log("Web3Modal instance is", web3Modal);
}

/**
 * Kick in the UI action after Web3modal dialog has chosen a provider
 */
export async function fetchAccountData() {
  console.log("FetchAccountData called");
  // Get a Web3 instance for the wallet
  const web3 = new Web3(provider);

  console.log("Web3 instance is", web3);

  // Get connected chain id from Ethereum node
  const chainId = await web3.eth.getChainId();

  const chainData = getChain(chainId);
  console.log("chainData", chainData);

  // Get list of accounts of the connected wallet
  const accounts = await web3.eth.getAccounts();

  // MetaMask does not give you all accounts, only the selected account
  console.log("Got accounts", accounts);
  const selectedAccount = accounts[0];

  console.log("selectedAccount ", selectedAccount);

  const balance = await web3.eth.getBalance(selectedAccount);
  // ethBalance is a BigNumber instance
  // https://github.com/indutny/bn.js/
  const ethBalance = web3.utils.fromWei(balance, "ether");
  const humanFriendlyBalance = parseFloat(ethBalance).toFixed(4);

  connectionData = {
    selectedAccount,
    accounts,
    humanFriendlyBalance,
    balance,
    chainData
  };

  return connectionData;
}

/**
 * Fetch account data for UI when
 * - User switches accounts in wallet
 * -
 * - User connects wallet initially User switches networks in wallet
 */
export async function refreshAccountData() {
  return await fetchAccountData();
}

export async function onConnect() {
  console.warn("Opening a dialog", web3Modal);
  let connectionData;
  try {
    provider = await web3Modal.connect();
    connectionData = await refreshAccountData();
  } catch (e) {
    console.warn("Could not get a wallet connection", e);
    return null;
  }
  return connectionData;
}

export async function disconnectChain() {
  try {
    await web3Modal.clearCachedProvider();
    await provider.disconnect();
  } catch (err) {
    console.warn("on disconnect error", err);
  }
}
