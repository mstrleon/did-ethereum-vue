import { mapState } from "vuex";

export const web3ModalMixin = {
  computed: {
    ...mapState(["web3m/web3Modal"]),
    active() {
      return this.web3Modal.active;
    }
  }
};

export const walletStates = {
  computed: {
    profileRead() {
      return (
        this.$store.state.app.status ===
        this.$store.state.app.appStates.PROFILE_READ
      );
    },
    walletConnected() {
      return (
        this.$store.state.app.status ===
        this.$store.state.app.appStates.CONNECTED
      );
    },
    walletDisonnected() {
      return (
        this.$store.state.app.status ===
        this.$store.state.app.appStates.DISCONNECTED
      );
    },
    loading() {
      return this.$store.state.app.loading;
    }
  }
};
