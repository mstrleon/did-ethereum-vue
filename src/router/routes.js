import { store } from "../store/index";

import { status, messages } from "../configs/status";
import QueryCard from "../components/cards/QueryCard.vue";
import ConnectWrapper from "../components/ConnectWrapper.vue";
import ExploreView from "../components/cards/ExploreView.vue";
import ProfileForm from "../components/cards/ProfileForm";
const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "edit",
        component: ProfileForm
      },
      {
        path: "explore",
        component: QueryCard
      },
      {
        path: "view",
        component: ExploreView
      },
      {
        path: "",
        component: ConnectWrapper
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
